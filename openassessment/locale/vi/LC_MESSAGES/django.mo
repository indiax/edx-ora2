��          �      |      �  $   �       �   3     �     �  	   �  C   �  
   ?     J     S     c     l     q     �  !   �  /   �  !   �     �       _     �   q  �  
  7   �  #   �  �   �  
   �     �     �  u   �     `     s     �     �     �     �     �  (   �  I   �  *   8	  
   c	     n	  h   �	  �   �	     	                                                                        
                    Additional comments on your response Assessments of Your Response Assignment submissions will close soon. To receive a grade, first provide a response to the question, then complete the steps below the <strong>Your Response</strong> field. Cancel Complete Course ID I disagree with one or more of the peer assessments of my response. Incomplete Location Peer Assessment Required Save Student Response Submit This response could not be saved. This response has been saved but not submitted. This response has not been saved. Value Your Response Your grade will be available when your peers have completed their assessments of your response. Your response is still undergoing peer assessment. After your peers have assessed your response, you'll see their comments and receive your final grade. Project-Id-Version: edx-platform
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-07-23 14:21-0400
PO-Revision-Date: 2014-07-23 18:24+0000
Last-Translator: Will Daly <will@edx.org>
Language-Team: Vietnamese (http://www.transifex.com/projects/p/edx-platform/language/vi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: vi
Plural-Forms: nplurals=1; plural=0;
 Nhận xét bổ sung về câu trả lời của bạn Đánh Giá Trả Lời Của Bạn Sắp hết hạn nộp bài tập. Để có điểm, trước hết hãy trả lời câu hỏi, sau đó hoàn thành các bước dưới mục <strong>Trả Lời Của Bạn</strong> field. Hủy bỏ Hoàn tất Mã Khóa Học Tôi không đồng tình với một hay các đánh giá của học viên khác về câu trả lời của mình. Chưa hoàn thành Địa chỉ Bình duyệt Bắt buộc Lưu Phản Hồi của Học Viên Gửi Không thể lưu câu trả lời này. Câu trả lời này đã được lưu nhưng chưa được gửi đi. Câu trả lời này chưa được lưu. Giá trị Trả Lời Của Bạn Bạn sẽ có điểm khi các học viên khác hoàn thành đánh giá câu trả lời của bạn. Câu trả lời của bạn vẫn đang được các học viên khác đánh giá. Sau khi họ đánh giá xong câu trả lời của bạn, bạn sẽ thấy nhận xét của họ và nhận được điểm số cuối cùng. 